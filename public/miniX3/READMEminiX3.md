**miniX 3 | Throbber**

![ScreenShot](screenshot_miniX3_1.png)

![ScreenShot](screenshot_miniX3_2.png)

https://SimoneLasborg.gitlab.io/ap-2020-minix/miniX3/

My overall inspiration came from the ability to let your mind wonder when you find yourself (typically) in a waiting position: waiting for the bus, the lights to turn green, in line at the super market or waiting on something to download or stream. It’s become quite normal to look at your phone whenever there’s just a few moments to “fill”. I think it’s a shame that we look at our phones so much - because we miss the chance for our minds to really wonder when we always feed it directly with content. I think it can be really meditative and inspirational to look at places, people, buildings, colors around us when we wait; you see things that makes you wonder, reflect or just makes you smile. This sort of mind wondering can really lead to random and funny thoughts and ideas. So I wanted to spark this mind wondering and create some kind of inspiration that would make the waiting person experience surprising and funny inputs during the waiting time.
I really wanted to turn the waiting time into something positive - something that in it self would be worth spending time on; not just because you are forced to but because you actually enjoy it.

As inspiration for what I could do to create something that would lead thoughts and ideas in surprising new ways I turned to a really silly kids show “KatjaKaj og BenteBent" I used to enjoy when I was younger. The creators of this show have a weird talent of combining random things into something really funny - I especially remember funny characters and animals from the cartoons.
From there I search for inspiration on funny animals - and I found this wonderful project that is so simple and so funny: https://www.thingsihavedrawn.com/.
I think it’s so amazing how something so simpel like this drawing project can be so funny, so I tried to achieve the same simplicity. From this I was inspired to make the random word generator that would combine a funny character trait and an animal; and by this create totally random, weird and funny combinations that can surprise and entertain.
I wanted it to happen all by it self - the waiting person shouldn’t have to do anything but to look at the screen for the effect to work - it happens almost automatically that you see the weird looking animals for your eyes as you read the random combined words. Of course you can be more or less interested but the program definitely invites you to think in new, random ways and to fill your waiting time with fun and inspiration.

Maybe it’s just my sense of humor, but I definitely find it way more fun to wait when I look at this word generator! Instead of the annoying and anxious waiting time I experienced a feeling of joy from the funny words and also excitement towards what silly combination would be the next one.
This is also the reason I added the moving dot; I wanted to create a feeling of suspense towards the upcoming combination. And also to give the person looking a sense rhythm in the shifting words so it wouldn’t feel to random and unpredictable.

To the coding part I wanted to try to create my own functions as we where introduced to in class. And I wanted to try out using conditional statements - for this I used Daniel Shiffman 3.1 (https://www.youtube.com/watch?v=1Osb_iGDdjk&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&t=0s). I also used video 3.2 (https://www.youtube.com/watch?v=LO3Awjn_gyU&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=15) to make the moving dot with a conditional statement (if).
Then I used references from p5 to make the two lines of text align next to each other: https://p5js.org/reference/#/p5/textAlign

I had troubles with the random function - this would sometimes (randomly off course) choose the same word two or three times in a row an thereby make the word generator seem out of sync. I got a lot of help from both Noah and Ann to figure this out and how to (kind of) solve it. But I’m still not completely satisfied with the result as it can still happen that the same word will be picked randomly twice in a row. It would be nice to find a way to completely solve this issue and not just decrease the probability of it happen.

And then I couldn’t figure out how to make the words appear instantly - the moving dot has to cross the screen 1 time to trigger the words to appear.
