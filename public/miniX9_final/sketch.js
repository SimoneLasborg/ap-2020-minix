//Used to make the mousepressed-function to only work inside the pictures
let picLeft;
let picRight;
let picTop;
let picButton;

//
let cookiesIsCracked = false;

//The quotes
let quote;

//The pictures of the cookies
let fortuneCookie;
let crackedFortuneCookie;

//Uploading of the images. The images is made by Mads
function preload() {
  fortuneCookie = loadImage('data/cookie.png');
  crackedFortuneCookie = loadImage('data/crackedcookie.png');
}

//This is the starting page with the whole cookie.
function setup () {
  createCanvas(windowWidth,windowHeight);
  background(250,90,20);
  imageMode(CENTER);
  image(fortuneCookie, width/2, height/2);
}

//This is where we use the variables of the picture to change it when pressed.
function mousePressed(event) {
  picLeft = width/2-559/2;
  picRight = width/2+559/2;
  picTop = height/2-518/2;
  picButton = height/2+518/2;
  //The coordinates of the picture does not match completely because the image is not square

//This is what makes the picture change if it is pressed inside the picture frame
  if (mouseX > picLeft && mouseX < picRight && mouseY > picTop && mouseY < picButton) {


    if(!cookiesIsCracked){//Check if the cookie already has been open
      background(250,90,20);
      imageMode(CENTER);
      image(crackedFortuneCookie, width/2, height/2,1000,400);
      loadJSON('https://api.adviceslip.com/advice',doSomethingWithQuote);
      cookiesIsCracked = true;
    }

    //The button that appears after pressing the cookie, so that you can get a new one
    buttonNewCookie = createButton('Unhappy with your fortune? Get a new!');
    buttonNewCookie.mousePressed(newCookie); //this direct you back to the whole cookie in setup
    buttonNewCookie.style("background","#ffd9b3"); //styling
    buttonNewCookie.style("border-radius", "12px"); //styling
    buttonNewCookie.position(width/3+50, height/2+255);
  }
}

//when the button is pressed it clears the page and goes back to setup, but we had to hide the button despite the clearing
function newCookie() {
      clear();
      setup();
      redraw();
      buttonNewCookie.hide();
      cookiesIsCracked = false;
}

//This is what makes the quotes appear on the note in the cookie.
function doSomethingWithQuote(q) {
  quote = q;
  textSize(20);
  noStroke();
  fill(0,0,0,160);
  textAlign(CENTER);
  rectMode(CENTER);

  text(quote.slip.advice, width/2, height/2+150, crackedFortuneCookie.width/5 ,400); //The quote.slip.advice is the path to the quotes in the url
}
