**Emoji no. 1 | Double chinned emoji**


![ScreenShot](screenshot_miniX2.png)


https://SimoneLasborg.gitlab.io/ap-2020-minix/miniX2/


My second emoji can be found in the folder miniX2.2


**Thoughts about mini exercise 2**

I chosed to make my two emojis from the base of a circle - made with the ellipse function. Before that I tried out both the vertex and the curveVertex function to draw a more complex figure. It took a lot of time to put in all the coordinates to each line manually and I wasn’t very happy with the result - it was quit a boxy and unfinished looked. I got a good sense of coordinates and placement in the canvas though - and the use of consol.log to find a position - so it was time well spent. But I decided to use the predefined figure functions for the emojis in stead. This way made it easier to achieve to look I wanted and I got familiar with many different functions in p5.

The first emoji has a cap that can change color when the mouse is pressed. It has sort of an awkward little smile and a double chin. 

The second one is a pretty classic round, yellow emoji - but this one has pimples…

I found this assignment really interesting - but also challenging! I felt a lot of pressure on deciding which emojis to make that would have something political or cultural to say. I finally chose to make the two yellow emojis because I find these kinds the most “neutral” ones. I think the ones that look like persons have created an ongoing dilemma of representation. It’s impossible to create emojis enough for everyone to feel represented; that be in gender, race, job function, activities, physique, body images and so on. By creating these emojis there’s suddenly a large group of people that feel left out - or directly ignored as a consequence of the narrow selection of human like emojis.

With the basics decided I then chose which expression I wanted my emojis to have. I wanted to make something fun. I think emojis should be used to reinforce positive emotions; to make a conversation lighter, more quirky and more fun. Not to underline negative and aggressive emotions. If you want to express dissatisfaction in writing - which is of course also needed - you need to do this with arguments in a proper tone; not by inserting angry or violent emojis. 
Of course emojis can always be interpreted in different ways dependent on the context they’re used in. But I tried to make two that was funny in their kind of awkward look and with their “imperfections” - the double chin and the pimples. The color shifting cap was an attempt to give the users of emojis a bigger impact on the look of them.