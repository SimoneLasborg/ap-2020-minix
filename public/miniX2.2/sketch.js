function setup() {
  createCanvas(400, 400);
}

function draw() {

  //gult ansigt
  noStroke();
  fill(255, 243, 150);
  ellipse(200,200,150,150);

  //øjne
  noStroke();
  fill(255)
  ellipse(178,185,20,25);
  ellipse(218,185,20,25);
  fill(0);
  ellipse(180,186,10,14);
  ellipse(216,186,10,14);

  //mund
  strokeWeight(3);
  noFill();
  stroke(194,4,4);
  arc(200,210,50,30,QUARTER_PI,PI-QUARTER_PI);

  //bums
  noStroke();
    //lyserød
  fill(258, 136, 156);
  ellipse(232,160,5,5);
  ellipse(216,150,10,10);
  ellipse(155,165,7,7);
    //gul
    fill(245, 194, 83);
    ellipse(216,150,5,5);
    ellipse(155,165,3,3);

//find koordinator
if(mouseIsPressed){
console.log(mouseX, mouseY);

}
}
