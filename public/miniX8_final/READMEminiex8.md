**Haiku Generator**

![ScreenShot](miniex8.png)

![ScreenShot](miniex81.png)

https://fredebering.gitlab.io/ap2020/All%20mini%20ex/miniex8



This program was made in collaboration with Simone 

**Provide a title of your work and a short description of the work (within 1000 characters)**


Our program is called Haiku Generator. Haiku is a traditional form of Japanese poetry and is relatively short. Haiku poems consist of three lines. The first and the last line have five syllables and the middle line has seven syllables. The lines rarely rhyme. Haiku poems are often about nature and usually about a specific season. It takes a lot of effort writing these types of poems. We chose to challenge this approach to Haiku poems as being something that is very perfect by making something that was completely random with no further thought and very messy. Whereas Haiku poems often have a greater meaning, ours have absolutely no meaning at all. 

We also made the decision to make the background a japanese cherry tree because we wanted to create an initial atmosphere that was calm and nice. A cherry tree is very beautiful and reminds us about the coming of spring. This calmness is disturbed when pressing the button “Generate new Haiku” and a completely random and meaningless poem appears.
In this way we make use of the connotations connected with the genre haiku and the beautiful cherry tree - we set a certain atmosphere only to “go against” this to make the disturbance even greater for the person interacting with the program. Hopefully this will create a moment of surprise and a humorous experience. 

**Describe your program in terms of how it works, and what you have used and learnt?**



When opening the program one sees a big, beautiful cherry tree along with a purple button saying “Generate a new Haiku”. When clicking the button a randomly generated Haiku poem appears on the screen. 
The button is created in setup and refers to a “self made” function when pressed: “function newHaiku”. This function first prints a new background (to make sure there's a clean sheet for each time the button is pressed) and then adds three lines of text. These lines are each selected randomly from three different arrays that are contained in a JSON file and loaded to the program in function preload. We’ve furthermore created a value in this function called tempFirst -Second and -Third. This is to minimize the risk of the same sentence getting picked randomly twice in a row from the array: the if statement used with this value checks if the new selected random sentence is identical to the one chosen just before (for instance if the sentence “flowers look pretty” is picked twice in a row) - if it is, the program should pick a new random one (this can then in fact still be the same sentence “flowers look pretty”, but the overall risk is still lower with this statement added to the function).

**Analyze and articulate your work:**


Analyze your own e-lit work by using the text Vocable Code and/or The Aesthetics of Generative Code (or other texts that address code/voice/language) in relation to code and language. And how would you reflect upon various layers of voices and/or the performativity of code in your program?
When creating code poetry or when working with e-lit one might focus on making the code the primary source of expression, where the code represents some deeper meaning. Here the running of the program is not the main focus, but the reading of the code is what creates the artwork and here the code is the primary focus. However we have chosen to focus on the outcome of the code: the program, and what we could do with this in relation to language. We chose a rather direct approach to the subject and created a poem-generator. 
Even though the program is only visual and doesn’t use sound or spoken words it still reflects upon words as something fleeting; as soon as you generate a new poem the former one will be gone - just as if you heard it read aloud. In this way the program operates around subjects like temporality, uniqueness and paying attention to the existing moment.
As Goeff Cox states in his article The Aesthetics of Generative Code the poem is only written and not read aloud, the reader can form the poem in her/his own way when reading it: “Poetry at the point of its execution (reading and hearing), produces meaning in multitudinous ways, and can be performed with endless variations of stress, pronunciation, tempo and style.” (line 18). It makes every generated haiku seem different to each reader. Furthermore the text by Cox et al. about The Aesthetics of Generative Code elaborates how “... poetry lies in the meeting of poem and reader, not in the lines of symbols printed on the pages of a book. What is essential is the aesthetic act” (s. 1). The very experience with the program (versus reading the poem in printed form, for instance) affects the reader’s interpretation of the poem. The temporal form and the reader’s opportunity to generate a new poem are both important factors for the experience with the haiku. 
Haiku poems can be a reflection about society and the perfectness we all try to be and achieve - Haiku Generator tries to shake it up and say that we should not take everything so serious all the time. This statement is highlighted in the very fact that the Haiku poems appear on a computer screen via a simple program. This makes the genre more approachable and informal. The randomly picked lines of already silly sentences further creates an expression of fun and a down-to-earth experience with poetry: it doesn’t have to be all that serious and heavy. Sometimes it is equally important to just let go and have fun with words. It can lighten up your mood and spark inspiration. 
The performative in this sense is, that the code performs a Haiku poem when the button is pressed. In the text Speaking Code Goeff Cox and Alex Maclean argues: “Speech acts come close to the way program code performs an action, like the instruction addressing the file. Programs are operative inasmuch as they do what they say, but moreover they do what they say at the moment of saying it.” (p. 35, l. 30). When creating a Haiku poem with pen and paper one has to think about the sentences and what they want to express, but what the Haiku Generator does is creating a Haiku poem instantly when pressing the button. This act of creating something instantly is what makes coding/programming performative. 