**miniX5 | Revisit the past**

![ScreenShot](screenshot_miniX5_2_1.png)

![ScreenShot](screenshot_miniX5_2_2.png)

https://SimoneLasborg.gitlab.io/ap-2020-minix/miniX5_2/

For this week’s mini exercise I started out looking at my earlier projects and which functions I’ve used. I listed a number of things that I’ve worked with up until now - I wanted to avoid too much repeating and to focus on the things I needed to practise more.

I found last weeks minix4 really difficult - I tried out a lot of DOM’elements and had a struggle with most of them. So I really wanted to explore these further in this minix5. I chose to experiment with buttons. At first I wanted to make a button that could show/hide a picture - but I couldn’t make the same button have these two functions (I experimented with remove, removeAttribute and return false;). So instead I created different buttons and let each of them contain a background - in this way they would reset the action from the button clicked beforehand. 

My program is a redesign of my minix3 - now with buttons to trigger the random animal word to appear rather than the time based (or more correctly frame count-based) throbber from before.
The button “randomize me an animal, plz” shows two randomly selected words from two arrays; the same as in minix3. The button “show me something silly” shows a randomly picked image from an array. The last button “I don’t wanna look at no animals anymore…” resets the screen by drawing a new background.

I’ve chosen to randomize the values for the two buttons so each outcome is unique and unpredictable (although there’s not so many pictures to select from…). I think it’s a funny perspective to the tendency with data capturing and tracking input online. As a contrast this program will give you a sense of not being able to go back - there’s no return button. You can only move forward to a new silly animal or reset the screen. You have to actually pay attention to what appears on your screen, you can’t get it back.
It's also a way of showing the (false) sense of security that we can get stuff back/return to something online. We, in most cases, don’t have the control of what is saved and what is removed.

My mini exercises have all been kind of silly - I think this is a fun way of playing around with programming and learning different functions. It keeps me motivated that I have fun during the proces. And to me it makes a lot of sense to be able to make something that is funny, surprising, can function as a break, can kickstart idea generation or creative thinking. 
And even though my projects have been somewhat childish or silly I’ve taken the practise and the thoughts behind very serious. I think it’s really motivating - and important - to learn how coding and programming works. It’s a growing part of so many aspects of our daily lives and it matters who designs the very core of technologies used - and what the motivation behind is (money, data, power, surveillance and so on).