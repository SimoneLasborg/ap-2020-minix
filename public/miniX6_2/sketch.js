//Defining the rover, the pictures, text font and a number of variables
let roverSize = {
  w:86,
  h:89
};
var roverPosX;

var fontwindows;
var moon;
var rover;
var bottle;

var screen = 0;
var y = -20;
var x = 200;
var speed = 2;
var score = 0;


//Loading the image for the background + the font used
function preload() {
  fontwindows = loadFont("ProFontWindows.ttf");
  moon = loadImage('moon.png');
  rover = loadImage('marsrover.png');
  bottle = loadImage('message2.png');
}


//Creating the canvas and resizing the pictures
function setup() {
  createCanvas(600, 400);
  bottle.resize(30,0);
  rover.resize(120,0);
  roverPosX = width/2;
}


//Draws the different screens + places the rover
function draw() {
  image(rover, 0, roverPosX, roverSize.w, roverSize.h);

	if(screen == 0){
    startScreen()
  } else if(screen == 1){
  	gameOn()
  } else if(screen==2){
  	endScreen()
  }
}


//The startscreen
function startScreen(){
  background(moon,0,0);
	textAlign(CENTER);
  textFont(fontwindows);
  textSize(35);
  fill(255);
	text('message from space', width/2, height/3);
  textSize(20);
  text('aliens are sending bottled messages from space', width/2, height/3+50);
  text('catch as many as possible with the rover', width/2, height/3+80);
  textSize(25);
	text('click to start', width/2, height/3+140);
	reset();
}


//The game
function gameOn(){
  background(moon,0,0);

  //Score count
  text("score = " + score,70,25)

  //Bottles moving + the rover following the mouse
  image(bottle, x, y);
  image(rover, mouseX, height-80);
	y+= speed;

  //If the bottle isn't cathed the screen turns to gameover
  if(y>height){
  	screen =2
	 }

  //The rover "catches" te bottles when they touch + adds 1 point + the speed increases
  if(y>height-10 && x>mouseX-20 && x<mouseX+80){
  	y=-20
    speed+=.5
    score+= 1
  }

  //Drop new bottle at a random position
	if(y==-20){
  	pickRandom();
  }
}

function pickRandom(){
	x= random(20,width-20)
}


//The gameover screen
function endScreen(){
    background(moon,0,0);
		textAlign(CENTER);
    textSize(30);
		text('GAME OVER', width/2, height/2);
    textSize(20);
  	text("SCORE = " + score, width/2, height/2 + 30);
		text('click to play again', width/2, height/2 + 90);
}


//When you click the mouse the screen will change
//startscreen -> play || gameover -> startscreen
function mousePressed(){
	if(screen==0){
  	screen=1
  }else if(screen==2){
  	screen=0
  }
}


//Resets the game and lets you start over
function reset(){
	  score=0;
  	speed=2;
  	y=-20;
}
