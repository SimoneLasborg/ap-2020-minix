let firstLine;
let secondLine;
let thirdLine;

let tempFirst;
let tempSecond;
let tempThird;

let alexFont;
let kaushanFont;

function preload (){
  words = loadJSON("data/words.json")
  tree = loadImage("data/cherry.jpg")
  alexFont = loadFont("data/AlexBrush-Regular.ttf");
  kaushanFont = loadFont("data/KaushanScript-Regular.otf");
}

function setup() {
  createCanvas(720, 700);
  background(tree);

  //Creating buttons
    buttonHaiku = createButton('Generate a new Haiku');
    buttonHaiku.position(280,560);
    buttonHaiku.mousePressed(newHaiku);
    buttonHaiku.style("color","#ffffff");
    buttonHaiku.style("background","#cc00cc");
    buttonHaiku.style("border", "none");
    buttonHaiku.style("font-size", "15px");
    buttonHaiku.style("border-radius", "8px");
    buttonHaiku.style("text-align", "CENTER")
  }

function draw() {
  //Header
  fill(255);
  noStroke();
  textSize(60);
  textFont(alexFont);
  textAlign(CENTER);
  text('Haiku generator', width/2, 150);
}

function newHaiku() {
  //Creates a 'clear' screen by drawing a new background for each time the button is pressed
  background(tree,0,0);

  //Picking random lines for the haiku from the JSON file
  //Making sure the risk for the same line picked twice in a row is lower
    tempFirst = firstLine;
    firstLine = random(words.fiveSyllables1);
    if(tempFirst == firstLine) {
      firstLine = random(words.fiveSyllables1);
    }

    tempSecond = secondLine;
    secondLine = random(words.sevenSyllables);
    if(tempSecond == secondLine) {
      secondLine = random(words.sevenSyllables);
    }

    tempThird = thirdLine;
    thirdLine = random(words.fiveSyllables2);
    if(tempThird == thirdLine) {
      thirdLine = random(words.fiveSyllables2);
    }

    //Text styling
    fill(255);
    noStroke();
    textSize(30);
    textFont(kaushanFont);
    textAlign(CENTER);
    text(firstLine, width/2, 300);
    text(secondLine, width/2, 350);
    text(thirdLine, width/2, 400);
}
