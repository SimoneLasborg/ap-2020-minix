//Defining the names for the sliders
let rSlider, gSlider, bSlider;

//Defining the variables for the functions that control the checkboxes
var tjekketA1, tjekketA2, tjekketA3;
var tjekketB1, tjekketB2, tjekketB3;
var tjekketC1, tjekketC2, tjekketC3;

//Defining the font
let Arcon;


function preload() {
  Arcon = loadFont('Arcon-Regular.otf');
}


function setup() {
  createCanvas(windowWidth, windowHeight);


//Create sliders
  rSlider = createSlider(0, 255, 60);
  rSlider.position(width/3, height/3);
  gSlider = createSlider(0, 255, 145);
  gSlider.position(width-350, height/2);
  bSlider = createSlider(0, 255, 145);
  bSlider.position(width/5, height-200);


//Create checkboxes for first question A
  checkboxA1 = createCheckbox('The blue one?', false);
  checkboxA1.position(width/3, height/3+30);
  checkboxA1.changed(myCheckedEventA1);

  checkboxA2 = createCheckbox('It has to be green', false);
  checkboxA2.position(width/3, height/3+60);
  checkboxA2.changed(myCheckedEventA2);

  checkboxA3 = createCheckbox('Yellow! I love yellow...', false);
  checkboxA3.position(width/3, height/3+90);
  checkboxA3.changed(myCheckedEventA3);


//Create checkboxes for second question B
  checkboxB1 = createCheckbox('Ehm what?', false);
  checkboxB1.position(width-350, height/2+30);
  checkboxB1.changed(myCheckedEventB1);

  checkboxB2 = createCheckbox('MUH', false);
  checkboxB2.position(width-350, height/2+60);
  checkboxB2.changed(myCheckedEventB2);

  checkboxB3 = createCheckbox('ÆØÅ', false);
  checkboxB3.position(width-350, height/2+90);
  checkboxB3.changed(myCheckedEventB3);


//Create checkboxes for third question C
  checkboxC1 = createCheckbox('I know this one! Red!', false);
  checkboxC1.position(width/5, height-200+30);
  checkboxC1.changed(myCheckedEventC1);

  checkboxC2 = createCheckbox('Still just blue..?', false);
  checkboxC2.position(width/5, height-200+60);
  checkboxC2.changed(myCheckedEventC2);

  checkboxC3 = createCheckbox('A shade of purple', false);
  checkboxC3.position(width/5, height-200+90);
  checkboxC3.changed(myCheckedEventC3);

}

function draw() {

//The changing background
  const r = rSlider.value();
  const g = gSlider.value();
  const b = bSlider.value();
  background(r, g, b);


//HEADER, "color detective"
  textSize(50);
  noStroke();
  fill(255);
  textFont(Arcon);
  textAlign(CENTER,CENTER);
  text('color detective', width/2, height/6);


//QUESTION A, "what color does this slider control?"
  textSize(20);
  noStroke();
  fill(255);
  textFont(Arcon);
  text('what color does this slider control?', width/3+50, height/3-30);


    //Checkboxes for question A
        if(tjekketA1){
          textSize(20);
          noStroke();
          fill(255);
          textFont(Arcon);
          text('try again!',200,200);
        } else {
          console.log('Unchecking');
        }

        if(tjekketA2){
          textSize(20);
          noStroke();
          fill(255);
          textFont(Arcon);
          text('no',500,500);
        } else {
          console.log('Unchecking');
        }

        if(tjekketA3){
          textSize(20);
          noStroke();
          fill(255);
          textFont(Arcon);
          text('not even close...',800,650);
        } else {
          console.log('Unchecking!');
      }


//QUESTION B, "name the 3 letters often used in defining colors"
  text('name the 3 letters often used in defining colors', width-350+50, height/2-30);

    //Checkboxes for question B
        if(tjekketB1){
          textSize(20);
          noStroke();
          fill(255);
          textFont(Arcon);
          text('one more try',1200,250);
        } else {
          console.log('Unchecking');
        }

        if(tjekketB2){
          textSize(20);
          noStroke();
          fill(255);
          textFont(Arcon);
          text('you´re silly',150,500);
        } else {
          console.log('Unchecking');
        }

        if(tjekketB3){
          textSize(20);
          noStroke();
          fill(255);
          textFont(Arcon);
          text('what´s that..?',1000,700);
        } else {
          console.log('Unchecking');
      }


//QUESTION C, "what color do you get when mixing blue and yellow?"
  text('what color do you get when mixing blue and yellow?', width/5+50, height-200-30);

    //Checkboxes for question C
        if(tjekketC1){
          textSize(20);
          noStroke();
          fill(255);
          textFont(Arcon);
          text('that´s not right',800,450);
        } else {
          console.log('Unchecking');
        }

        if(tjekketC2){
          textSize(20);
          noStroke();
          fill(255);
          textFont(Arcon);
          text('you´re getting closer!',350,350);
        } else {
          console.log('Unchecking!');
        }

        if(tjekketC3){
          textSize(20);
          noStroke();
          fill(255);
          textFont(Arcon);
          text('still not',1250,750);
        } else {
          console.log('Unchecking!');
      }

}


//Checkboxes for the first question A
  function myCheckedEventA1() {
    tjekketA1 = this.checked();
  }

  function myCheckedEventA2() {
    tjekketA2 = this.checked();
  }

  function myCheckedEventA3() {
    tjekketA3 = this.checked();
  }

//Checkboxes for the second question B
  function myCheckedEventB1() {
    tjekketB1 = this.checked();
  }

  function myCheckedEventB2() {
    tjekketB2 = this.checked();
  }

  function myCheckedEventB3() {
    tjekketB3 = this.checked();
  }

//Checkboxes for the third question C
  function myCheckedEventC1() {
    tjekketC1 = this.checked();
  }

  function myCheckedEventC2() {
    tjekketC2 = this.checked();
  }

  function myCheckedEventC3() {
    tjekketC3 = this.checked();
  }


//To make sure that the canvas resizes when the window is resized.
  function windowResized(){
    resizeCanvas(windowWidth,windowHeight);
  }
