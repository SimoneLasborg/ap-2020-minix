**miniX10 | Algorithms and flowcharts**


**Individual flowchart**

For the individual part of this week’s assignment about flowcharts I’ve chosen to make a flowchart based on my miniX 3 (visit this link to see it https://simonelasborg.gitlab.io/ap-2020-minix/miniX3/).
The program is a throbber designed to entertain the viewer during the wait by presenting a randomly picked adjective paired with a randomly picked animal (which often results in quite funny matches).

![ScreenShot](flowchart_miniX3.png)


**Group flowchart**

We came up with two programs for the group assignment; one exploring gender and salary and one about sexuality and rights. Both programs adresses the topic og gender and sexuality in today’s society and how there’s still very much a difference in for instance salary and rights. The two programs both highlight an existing inequality by creating a caricatured image of (some of) your life conditions based on either your gender or your sexuality.
The programs are further designed to clarify just how non-neutral a computer program can be based on which algorithms it works upon - and which values it therefor works within. By extracting data from the internet (about salary or rights) and comparing them with very limiting definitions defined by the program (about gender or sexuality), the program will present to you a “truth” about your life. This obscure and obviously exaggerated conclusion will (hopefully!) make the user of the program reflect upon how the program is designed and also how data from another source is being interpreted and presented by the program.


*Gender and salary*

![ScreenShot](gender_and_salary_flowchart.png)

The program starts out with defining your gender by using a simple face recognition-technology. Based on your gender the program will then show what your average salary should be. This is based on numbers from Danmarks Statistik (dst.dk). If you’re a woman the program will tell you that you could get a much higher salary being a man - and it then refers you to a website with information about gender change.
This program first of all highlights unequal conditions in pay rates considering gender. But it does so by interpreting numbers from dst.dk in a very simplified way. Moreover the program’s way of defining your gender is based on a technology which the user is not sure what is based on; what constitutes a female or a male face? Is it the size of your eyebrows, the length of your hair, which view on gender (and beauty ideals) is incorporated in the algorithm? The program only works within two genders which is also very excluding to people who doesn’t characterize themselves as one of these two.
And talking about machine learning, how is this technology, this apparently “artificial intelligence”, trained to guess a person’s gender? How many - and which - people makes the foundation of the program?

This is both a political and a technical challenge that we would face designing this program. It would be a huge task to develop a face recognition program from scratch but by using an existing one we have to be aware how this has been built (referring to the problematics described above). These underlaying conditions for the program would be important to be transparent about; to make the user aware of the data collection the program works upon and how this might affect the result. If we choose to incorporate only two genders in the program it would likewise be important to be open about this choice: to explain the user that we have deliberately chosen only these two in order to make the program function even though we are fully aware of - and respect - that there’s more than 60 (defined) genders. It’s important that users see the program for what it is: an over simplified view on a binary gendered labour market that is created with a sense of irony to address a serious and complex issue


*Sexuality and rights*

![ScreenShot](sexuality_and_rights_flowchart.png)

What we wanted to criticize with this program is the huge inequality that is between different sexualities. Also the limiting binary choice of hetero or not, as a problematic convension and norm that still needs attention around the world.
It is also a comment on a caricatured view on a heteronormative relationship; a view that is still very much present and “locks” people in certain roles based on expectations to their gender.
This program is meant to tell a story about one's life based on different data inputs which the subject gives the program and the program will then tell you a fairytale about your life based on different parameters which are changeable.
This program makes you choose your sexuality (whether or not you are hetero) and tells you a fairytale based on your input. The fairytale will be affected by the sexality and other info you give to the program.
You are only giving the choice of being hetorosexual or not. So anything but hetoresexual will be treated in another way. If you're heterosexual you are presented with a straight forward, heteronormative story about your life - and if you’re anything else you will be presented with all kinds of challenges and misery (based on real conditions about unequal rights, repression and so on).
These different endings show how one’s life can have a different outcome based on sexuality.
One of the technical difficulties that could occur is collecting data from the different countries and their laws about sexuality and putting it all into our own JSON-file. We also have to figure out if it makes more sense to have many different JSON-files in order to better organize our data or to put it in fewer JSON-files.


**What are the challenges between simplicity at the level of communication and complexity at the level of algorithmic procedure?**

When creating the flowcharts it was difficult to choose what to incorporate - how many details should be presented and how much of the syntax should be explained? Both as a group and by my self I tried to incorporate an amount of detail that would make the reader of the flowchart understand how the program works; on what input, data or algorithmic conditions it moves from one state to another in the program. I tried to leave out the technical details about the specific line of code or syntax used, so that this “code language” wouldn’t confuse the reader.


**What is the value of the individual and the group flowchart that you have produced?**

When we worked together as a group it was much faster to explore all the different layers of the program we needed to present in the flowchart. But it also made the process a bit more rushed and superficial; you can quickly rely on the others to detect problems or missing links and this will make you not as observant. On my own I was forced in another way to look very closely to my program to make sure that I got all the aspects I wanted presented in my flowchart. It was far more time consuming and a bit more difficult to get started on on my own, but I think the result is more detailed. 
