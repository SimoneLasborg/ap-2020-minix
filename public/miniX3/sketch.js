
let sillyword = ['discodansing', 'one legged', 'drooling', 'astronaut', 'glow-in-the-dark', 'flying', 'underwater', 'furry', 'cockeyed', 'spinning', 'blinking', 'opera singing', 'hairless', 'pantswearing', 'sneezing', 'hiccuping', 'staring', 'moonwalking', 'beeping', 'robot', 'rainbow colored', 'rollerblading', 'diaper-wearing', 'icecream-eating'];
let tempFirst = "";
let firstword = "";

let animalword = ['gnu', 'meerkat', 'donkey', 'owl', 'lizard', 'sloth', 'flamingo', 'starfish', 'jellyfish', 'lemur', 'rattlesnake', 'parrot', 'octopus', 'sea lion', 'raccoon', 'hummingbird', 'beaver', 'moose', 'orangutan', 'fox', 'worm', 'centipede', 'hippo', 'dromedary', 'zebra'];
let tempSecond = "";
let secondword = "";

var x = 0;

function setup() {
createCanvas(windowWidth, windowHeight);
//the framerate sets the tempo of the moving dot
frameRate(50);
}

function draw() {
//overwriting with a solid background
background(190, 249, 250);

//calling the functions that I define later
dot();
sillyness();
animal();
}

//the moving dot
function dot(){
noStroke();
fill(24, 205, 214);

ellipse(x,windowHeight/1.5,30,30);
x = x + 3;

if(x>width){
x = 0;
  }
}

//function for the first word - random generated silly words
function sillyness(){
/*To lower the chance that the same word from the array is picked twice in a row I've made the if statement with the tempFirst.
This checks if the newly chosen word is the same as the last one.
If it is it will choose a new one from the list.
This new word no. 2 can then still be the same as the former one but the probability is lower*/
if(x==0){
tempFirst = firstword;
firstword = random(sillyword)
  if(tempFirst == firstword){
  firstword = random(sillyword)
  }
}

fill(15, 181, 184);
noStroke();
textSize(30);
textAlign(RIGHT, CENTER);
text(firstword, width/2, height/2);
}

//function for the second word - random generated animals
function animal(){
if(x==0){
tempSecond = secondword;
secondword = random(animalword)
  if(tempSecond == secondword){
  secondword = random(animalword)
  }
}

fill(63, 242, 221);
noStroke();
textSize(30);
textAlign(LEFT, CENTER);
text(secondword, width/2, height/2);
}

//To make sure that the canvas resizes when the window is resized.
function windowResized(){
  resizeCanvas(windowWidth,windowHeight);
}
