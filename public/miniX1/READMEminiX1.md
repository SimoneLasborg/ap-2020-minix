**mini exercise 1**

**How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?**
I’ve had a really positive first experience with coding. At first I didn’t get anything when I looked at codes.
It really was a completely different and foreign language to me. I was pretty overwhelmed with all the different words I didn’t understand and especially all the weird-looking signs confused me.
After reading about coding and trying out some simple ones myself I have already gained much more confidence in understanding code. I now have an idea about syntax, values and special signs - and I can kind of make sense of the whole idea behind. 
It has worked really well for me to search for specific shapes, copying the code and then changed the values to see what effect the values had.
Also to move the different syntaxes around and changing the order of them have proven to be a good exercise that have taught me about the hierarchy in coding. 

**How is the coding process different from, or similar to, reading and writing text?**
The reading of code can be compared to the reading of text in the way that I’ve read a code, tried to understand it and then tried to reproduce it in a specific context. 

**What does code and programming mean to you, and how do the assigned readings help you to reflect on programming?**
Before this course programming was really foreign to me. I knew that it controlled technology around me but I didn’t really get how - and I didn’t realize just how biased programming can really be.
Especially the readings for the first class helped me to realize just how important programming is - not only in scientific fields but in culture and society as well.
I think this is so interesting and very motivating for learning how to programme - or at least to understand how it works and what elements that lies within programming.


https://SimoneLasborg.gitlab.io/ap-2020-minix/miniX1/


![ScreenShot](Screenshot_miniX1.png)

