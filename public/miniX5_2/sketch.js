//Defining the font
let Arcon;

//Defining the words for the generator
let sillyword = ['discodansing', 'one legged', 'drooling', 'astronaut', 'glow-in-the-dark', 'flying', 'underwater', 'furry', 'cockeyed', 'spinning', 'blinking', 'opera singing', 'hairless', 'pantswearing', 'sneezing', 'hiccuping', 'staring', 'moonwalking', 'beeping', 'robot', 'rainbow colored', 'rollerblading', 'diaper-wearing', 'icecream-eating'];
let animalword = ['gnu', 'meerkat', 'donkey', 'owl', 'lizard', 'sloth', 'flamingo', 'starfish', 'jellyfish', 'lemur', 'rattlesnake', 'parrot', 'octopus', 'sea lion', 'raccoon', 'hummingbird', 'beaver', 'moose', 'orangutan', 'fox', 'worm', 'centipede', 'hippo', 'dromedary', 'zebra'];
let firstword = "";
let secondword = "";

//Defining pictures for "show me something silly"
let sillyPic = [];

//Loading the font and the pictures + defining the pictures in an array
function preload() {
  Arcon = loadFont("Arcon-Regular.otf");
  sillyPic[0] = loadImage("bird_seal.png");
  sillyPic[1] = loadImage("camel_swan.png");
  sillyPic[2] = loadImage("chubby_giraf.png");
  sillyPic[3] = loadImage("duck_horse.png");
  sillyPic[4] = loadImage("lion.png");
  sillyPic[5] = loadImage("monky_dog.png");
  sillyPic[6] = loadImage("penguin_whale.png");
  sillyPic[7] = loadImage("rabbit_bird.png");
  sillyPic[8] = loadImage("shark_horse.png");
}

//Setting up the canvas
function setup() {
  createCanvas(windowWidth, windowHeight);
  background(49,153,148);

//Creating buttons
  buttonrandomize = createButton('randomize me an animal, plz');
  buttonrandomize.position(400, height/4);
  buttonrandomize.mousePressed(randomAnimal);

  buttonsilly = createButton('show me something silly');
  buttonsilly.position(width-500, height/4);
  buttonsilly.mousePressed(sillyPicture);

  buttonreset = createButton('I don´t wanna look at no animals anymore...');
  buttonreset.position(width/2.5, height/1.2);
  buttonreset.mousePressed(reset);
}

function draw() {
//Header
  noStroke();
  fill(255);
  textSize(30);
  textFont(Arcon);
  textAlign(CENTER,CENTER);
  text('funny animal generator',width/2, height/6);
}

function randomAnimal() {
//Creates a 'clear' screen by drawing a new background for each time the button is pressed
  background(49,153,148);

//Picking two random words from the arrays
  firstword = random(sillyword);
  secondword = random(animalword);

  fill(15, 181, 184);
  noStroke();
  textSize(30);
  textAlign(RIGHT, CENTER);
  text(firstword, width/2, height/2);

  fill(63, 242, 221);
  noStroke();
  textSize(30);
  textAlign(LEFT, CENTER);
  text(secondword, width/2, height/2);
}

function sillyPicture() {
//Creates a 'clear' screen by drawing a new background for each time the button is pressed
  background(49,153,148);

//Picking a random picture from the array
  imageMode(CENTER);
  randomPic = random(sillyPic);
  randomPic.resize(width/4, width/4);
  image(randomPic, width/2, height/2.5+100);
}

function reset() {
//Creates a 'clear' screen by drawing a new background for each time the button is pressed
  background(49,153,148);
}

//To make sure that the canvas resizes when the window is resized.
  function windowResized() {
    resizeCanvas(windowWidth,windowHeight);
}
