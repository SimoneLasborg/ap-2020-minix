//definér x-positionen, hvis der skal være bevægelse
let xPos = 0;

function setup() {
  createCanvas(1250, 650);
  noFill();
 stroke(220);
 strokeWeight(2);
}

function draw() {
background(5, 5, 82);

//stjerneform
noStroke();
fill(245, 216, 29);
  push();
  translate(width * 0.05, height * 0.1);
  //stjernes placering på canvas??
  rotate(frameCount / -100.0);
  //hastighed for stjernens rotation
  star(2, 2, 10, 20, 5);
  //stjernes placering på canvas?? + str., str. og antal 'arme'
  pop();

  push();
  translate(width * 0.25, height * 0.5);
  rotate(frameCount / -100.0);
  star(2, 2, 10, 20, 5);
  pop();

  push();
  translate(width * 0.1, height * 0.7);
  rotate(frameCount / -100.0);
  star(2, 2, 10, 20, 5);
  pop();

  push();
  translate(width * 0.4, height * 0.3);
  rotate(frameCount / -100.0);
  star(2, 2, 10, 20, 5);
  pop();

  push();
  translate(width * 0.4, height * 0.9);
  rotate(frameCount / -100.0);
  star(2, 2, 10, 20, 5);
  pop();

  push();
  translate(width * 0.65, height * 0.8);
  rotate(frameCount / -100.0);
  star(2, 2, 10, 20, 5);
  pop();

  push();
  translate(width * 0.5, height * 0.65);
  rotate(frameCount / -100.0);
  star(2, 2, 10, 20, 5);
  pop();

  push();
  translate(width * 0.7, height * 0.4);
  rotate(frameCount / -100.0);
  star(2, 2, 10, 20, 5);
  pop();

  push();
  translate(width * 0.9, height * 0.6);
  rotate(frameCount / -100.0);
  star(2, 2, 10, 20, 5);
  pop();

  push();
  translate(width * 0.8, height * 0.3);
  rotate(frameCount / -100.0);
  star(2, 2, 10, 20, 5);
  pop();

  fill(45,246,252);
  ellipse(mouseX, mouseY, 100, 100);
  ellipse(mouseX, mouseY, 200, 30);

//bevægelse ud fra x-position og derefter figuren
xPos = xPos -0.5
if(xPos<0){
xPos = height;
}
fill(176, 252, 255)
  ellipse(xPos,100,60,60);
  ellipse(xPos,100,100,20);

}

function star(x, y, radius1, radius2, npoints) {
  let angle = TWO_PI / npoints;
  let halfAngle = angle / 2.0;
  beginShape();
  for (let a = 0; a < TWO_PI; a += angle) {
    let sx = x + cos(a) * radius2;
    let sy = y + sin(a) * radius2;
    vertex(sx, sy);
    sx = x + cos(a + halfAngle) * radius1;
    sy = y + sin(a + halfAngle) * radius1;
    vertex(sx, sy);
  }
  endShape(CLOSE);
}
