**miniX 4 | Capture All**

![ScreenShot](screenshot_miniX4_4_1.png)

![ScreenShot](screenshot_miniX4_4_2.png)

![ScreenShot](screenshot_miniX4_4_3.png)

https://SimoneLasborg.gitlab.io/ap-2020-minix/miniX4_4/

I think this assignment raised a lot of interesting questions; like which data are being tracked about us and how it is used. I think about this a lot in my everyday life when I use services, buy things or read and watch stuff online. I care about that my choices are being tracked and that this affects which adds and content in generel I’m being presented for online. I remember seeing a TedTalk about “The tribes we live in” that is exactly about this tendency; that whenever we click on something, this data is being kept and used to predict what we - according to some algorithm - might also like. In this way we’re constantly being presented stuff we already know or agree with and this keeps us in a “tribe” or a bubble. This data tracking and personalized experience of the web reinforces our existing preferences and can be a part of keeping us from challenging or provocative content that could possibly broaden our knowledge and horizon. 

This is a big concern for me when I use the net; I think it is really important to be confronted with a lot of different things and perspectives that might not be something I like or agree with to make sure to get a broad perspective - on all kinds of stuff from movies, music, politics, news, trends and so on. 

With this in mind I wanted to create something that could express this judgement that we experience through our selections online. I wanted to create a simple program that would jump to conclusions based on just simple interaction. 
I struggled a lot with different DOM-elements; I tried out keyPressed/keyIsPressed, createSelect (a dropdown menu) and createColorPicker. I made some of them work but had a lot of problems with getting them to work together. Especially the conditional statements where difficult and complex. 

So I ended up making something very simple that would highlight the experience of having only a few predefined opportunities for answering questions online - typically when you sign up for something or go through a questionary. This is often oversimplified and not representative. You can experience being “forced” to choose answers that doesn’t really reflect your opinion - and this answer is then kept and used to form your experience online (as commented before). 
The program is made with a few key elements - I ended up focusing on especially the createCheckbox and createSlider to really get familiar with the tools and how to adjust the values. It isn’t what I wanted it to be, but I still had a lot of fun playing around with the many different DOM-elements and I will definetely continue to explore them. 

