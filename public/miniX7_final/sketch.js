let lines = [];
let r,g,b;
let angle = 0;

class Lines {
  constructor(x,y,h) {
    this.x = random(width);
    this.y = random(height);
    this.h = random(400);
    r = random(255);
    g = random(255);
    b = random(255);
  }

  move() {
    rotate(angle);
    angle = angle + 1;
    this.x = this.x + angle + 1;
    this.y = this.y + angle + 1;
  }
  show() {
    noStroke();
    fill(r,g,b);
    rectMode(CENTER);
    rect(this.x, this.y, 1, this.h);
  }
}

function setup() {
  createCanvas(800,600);
  background(255,250,236);
  frameRate(2);
  angleMode(DEGREES);
}

function draw() {

  for (let i = 0; i < lines.length; i++) {
  lines[i].move();
  lines[i].show();
  }

  for (let i = 0; i < 1; i++) {
    let x = random(width);
    let y = random(height);

    lines[i] = new Lines(x,y);
  }
}
