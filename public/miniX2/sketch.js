let r, g, b;

function setup() {
  createCanvas(400, 400);

// Pick colors randomly
r = random(255);
g = random(255);
b = random(255);
}

function draw() {
//gult ansigt
noStroke();
fill(255, 242, 125);
ellipse(200,200,150,150);

//kanten på kasketten
strokeWeight(8);
stroke(r, g, b);
line(132, 170, 300, 145);

//kasket
fill(r, g, b);
arc(195, 168, 125, 100, radians(180), radians(-18), CHORD);
//ellipsens, som arc'en består af, koordinator er i midten af midten
//arc(x, y, bredde, højde, radians(tal) regner grader om til radianer)

//øjne
noStroke();
fill(255)
ellipse(178,185,20,25);
ellipse(218,185,20,25);
fill(0);
ellipse(180,186,10,14);
ellipse(216,186,10,14);

//mund
strokeWeight(3);
noFill();
stroke(194,4,4);
arc(200,200,50,30,QUARTER_PI,PI-QUARTER_PI);

//dobbelthage
stroke(242, 210, 48);
arc(200,220,40,20,QUARTER_PI,PI-QUARTER_PI);

//farveskiftende funktion
if (mouseIsPressed) {
  // Pick new random color values
  r = random(255);
  g = random(255);
  b = random(255);
  }

if(mouseIsPressed){
console.log(mouseX, mouseY);
}

}
